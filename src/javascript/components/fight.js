import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    if (getWinner(first, second)) {
      const winner=first.currentHealth <= 0 ? secondFighter : firstFighter;
      resolve(winner);
    }

    // resolve the promise with the winner when fight is over
  });
}

function getWinner(first, second) {
  if (first.currentHealth <= 0) {
    return second;
  } else if (second.currentHealth <= 0) {
    return first;
  } else {
    return false;
  }
}

export function getDamage(attacker, defender) {
  const damage=getHitPower(attacker)-getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance=randomNumber(1, 2);
  return attack*criticalHitChance;
}

export function getBlockPower(defence) {
  const dodgeChance=randomNumber(1, 2);
  return defense*dodgeChance;
}

function randomNumber(minimum, maximum){
  return Math.random()*(maximum-minimum)+minimum;
}